<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegistrasiController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\SiswaController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user.home');
});


Route::get('login',[LoginController::class, 'index'])->name('login');
Route::post('login',[LoginController::class, 'authenticate'])->name('login');
Route::get('registrasi',[RegistrasiController::class, 'index'])->name('registrasi');
Route::post('registrasi',[RegistrasiController::class, 'store'])->name('registrasi');
Route::post('logout', [RegistrasiController::class, 'logout']);

Route::get('home',[UserController::class, 'index'])->name('home');
Route::get('homeadmin',[UserController::class, 'homeadmin'])->name('homeadmin');
Route::get('dataguru',[UserController::class, 'dataguru'])->name('dataguru');
Route::get('datasiswa',[UserController::class, 'datasiswa'])->name('datasiswa');
Route::get('datanilai',[UserController::class, 'datanilai'])->name('datanilai');

Route::get('tambahdataguru', [GuruController::class,'index']);
Route::post('tambah_guru', [GuruController::class,'store']);
Route::get('editguru/{id}', [GuruController::class,'edit']);
Route::get('hapusguru/{id}', [GuruController::class,'destroy']);
Route::put('simpaneditguru/{id}', [GuruController::class,'update']);

Route::get('tambahdatasiswa', [SiswaController::class,'index']);
Route::post('tambah_siswa', [SiswaController::class,'store']);
Route::get('editsiswa/{id}', [GuruController::class,'edit']);
Route::get('hapussiswa/{id}', [SiswaController::class,'destroy']);
Route::put('simpaneditsiswa/{id}', [SiswaController::class,'update']);

