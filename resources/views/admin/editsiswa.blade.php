<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <nav class="navbar navbar-dark bg-dark py-3" id="mainNav">
      <div class="container px-4 px-lg-5">
          <a class="navbar-brand" href="#page-top"><i class="bi bi-book"></i> e-Raport</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav ms-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="homeadmin">Home</a></li>
                  <li class="nav-item"><a class="nav-link" href="{{url('tambahdataguru')}}">Tambah Data Guru</a></li>
                  <li class="nav-item"><a class="nav-link" href="{{url('tambahdatasiswa')}}">Tambah Data Siswa</a></li>
                  <li class="nav-item"><a class="nav-link" href="home">Logout</a></li>
                  
              </ul>
          </div>
      </div>
  </nav>
  <!-- Masthead-->
  <header class="masthead">
      <div class="container px-4 px-lg-5 h-100">
          <div class="row gx-4 gx-lg-5 h-100 align-items-center justify-content-center text-center">
              <div class="col-lg-8 align-self-end">
                  <h1 class="text-white font-weight-bold">e-Raport</h1>
                  <hr class="divider" />
              </div>
          </div>
      </div>
  </header>
  <!-- About-->
  <form action="{{url('simpaneditsiswa',$tambah->id)}}" method="POST">
      @csrf
      @method('put')
      <div class="mb-3">
        <label for="nama" class="form-label">Nama Lengkaap</label>
        <input type="text" class="form-control" id="nama" name="nama" value="{{$tambah->nama}}">
      </div>
      <div class="mb-3">
        <label for="alamat" class="form-label">Alamat</label>
        <input type="text" name="alamat" class="form-control" id="alamat" value="{{$tambah->alamat}}">
      </div>
      <div class="mb-3">
        <label for="tetala" class="form-label">Tetala</label>
        <input type="text" name="tetala" class="form-control" id="tetala" value="{{$tambah->tetala}}">
      </div>
      <div class="mb-3">
        <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
        <input type="text" name="jenis_kelamin" class="form-control" id="jenis_kelamin" value="{{$tambah->jenis_kelamin}}">
      </div>
      <div class="mb-3">
        <label for="no_telp" class="form-label">No. Telepon</label>
        <input type="text" name="no_telp" class="form-control" id="no_telp" value="{{$tambah->no_telp}}">
      </div>
      <div class="mb-3">
        <label for="p_a_b" class="form-label">Pendidikan Agama dan Budipekerti</label>
        <input type="text" name="p_a_b" class="form-control" id="p_a_b" value="{{$tambah->p_a_b}}">
      </div>
      <div class="mb-3">
        <label for="pkn" class="form-label">Pendidikan Kewarganegaraan</label>
        <input type="text" name="pkn" class="form-control" id="pkn" value="{{$tambah->pkn}}">
      </div>
      <div class="mb-3">
        <label for="b_indo" class="form-label">Bahasa Indonesia</label>
        <input type="text" name="b_indo" class="form-control" id="b_indo" value="{{$tambah->b_indo}}">
      </div>
      <div class="mb-3">
        <label for="b_inggris" class="form-label">Bahasa Inggris</label>
        <input type="text" name="b_inggris" class="form-control" id="b_inggris" value="{{$tambah->b_inggris}}">
      </div>
      <div class="mb-3">
        <label for="mtk" class="form-label">Matematika</label>
        <input type="text" name="mtk" class="form-control" id="mtk" value="{{$tambah->mtk}}">
      </div>
      <div class="mb-3">
        <label for="ipa" class="form-label">Ilmu Pengetahuan Alam</label>
        <input type="text" name="ipa" class="form-control" id="ipa" value="{{$tambah->ipa}}">
      </div>
      <div class="mb-3">
        <label for="ips" class="form-label">Ilmu Pengetahuan Sosial</label>
        <input type="text" name="ips" class="form-control" id="ips" value="{{$tambah->ips}}">
      </div>
      <div class="mb-3">
        <label for="seni" class="form-label">Seni Budaya</label>
        <input type="text" name="seni" class="form-control" id="seni" value="{{$tambah->seni}}">
      </div>
      <div class="mb-3">
        <label for="penjas" class="form-label">Penjaskes</label>
        <input type="text" name="penjas" class="form-control" id="penjas" value="{{$tambah->penjas}}">
      </div>
     
      <button type="submit" class="btn btn-dark">Submit</button>
    </form>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>

