<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->id();
            $table->string('nama', 100);
            $table->string('alamat', 100);
            $table->string('tetala', 100);
            $table->string('jenis_kelamin', 100);
            $table->string('no_telp', 100);
            $table->string('p_a_b', 100);
            $table->string('pkn', 100);
            $table->string('b_indo', 100);
            $table->string('mtk', 100);
            $table->string('ipa', 100);
            $table->string('ips', 100);
            $table->string('seni', 100);
            $table->string('penjas', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
