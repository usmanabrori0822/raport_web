<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\siswa;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tambah=siswa::all();
        return view('admin.tambahdatasiswa', compact('tambah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        siswa::create([
            'nama' => $request-> nama,
            'alamat' => $request-> alamat,
            'tetala' => $request-> tetala,
            'jenis_kelamin' => $request-> jenis_kelamin,
            'no_telp' => $request-> no_telp,
            'p_a_b' => $request-> p_a_b,
            'pkn' => $request-> pkn,
            'b_indo' => $request-> b_indo,
            'mtk' => $request-> mtk,
            'ipa' => $request-> ipa,
            'ips' => $request-> ips,
            'seni' => $request-> seni,
            'penjas' => $request-> penjas,
        ]);
        return redirect('tambahdatasiswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tambah=siswa::all();
        return view('admin.editsiswa', compact('tambah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = siswa::find($id);
        $save = $data->update([
            'nama' => $request-> nama,
            'alamat' => $request-> alamat,
            'tetala' => $request-> tetala,
            'jenis_kelamin' => $request-> jenis_kelamin,
            'no_telp' => $request-> no_telp,
            'p_a_b' => $request-> p_a_b,
            'pkn' => $request-> pkn,
            'b_indo' => $request-> b_indo,
            'mtk' => $request-> mtk,
            'ipa' => $request-> ipa,
            'ips' => $request-> ips,
            'seni' => $request-> seni,
            'penjas' => $request-> penjas,

        ]);
        if($save){
            $dtpesan = siswa::all();
            return redirect()->route('tambahdatasiswa', compact('dtpesan'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = siswa::findorfail($id);
        $data->delete();
        return back();
    }
}
