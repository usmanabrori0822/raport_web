<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\guru;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tambah=guru::all();
        return view('admin.tambahdataguru', compact('tambah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        guru::create([
            'nama' => $request-> nama,
            'alamat' => $request-> alamat,
            'tetala' => $request-> tetala,
            'jenis_kelamin' => $request-> jenis_kelamin,
            'no_telp' => $request-> no_telp,
        ]);
        return redirect('tambahdataguru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tambah=guru::all();
        return view('admin.editguru', compact('tambah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = guru::find($id);
        $save = $data->update([
            'nama' => $request-> nama,
            'alamat' => $request-> alamat,
            'tetala' => $request-> tetala,
            'jenis_kelamin' => $request-> jenis_kelamin,
            'no_telp' => $request-> no_telp,
        ]);
        if($save){
            $dtpesan = guru::all();
            return redirect()->route('tambahdataguru', compact('dtpesan'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = guru::findorfail($id);
        $data->delete();
        return back();
    }
}
