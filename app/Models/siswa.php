<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
    protected $table = "siswas";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama','alamat','tetala','jenis_kelamin','no_telp','p_a_b','pkn','b_indo','b_inggris','mtk','ipa','ips','seni','penjas'
    ];
}
