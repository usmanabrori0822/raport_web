<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class guru extends Model
{
    protected $table = "gurus";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama','alamat','tetala','jenis_kelamin','no_telp'
    ];
}
